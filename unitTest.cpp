#include <tuple>
#include "tupleUtils.h"
using namespace std;

int main() {
    auto a = zip(make_tuple(1, 4), make_tuple(2, 3, 5));
    auto a0 = get<0>(a), a1 = get<1>(a);
    printf("a0: %d %d\n", get<0>(a0), get<1>(a0));
    printf("a1: %d %d\n", get<0>(a1), get<1>(a1));
    auto b = tuple_flatten(make_tuple(make_tuple(1, 4),
                                      make_tuple(2, 3, make_tuple(5, 6))));
    printf("b: %d %d %d %d %d %d\n", get<0>(b), get<1>(b), get<2>(b),
                                     get<3>(b), get<4>(b), get<5>(b));
    return 0;
}
