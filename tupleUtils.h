#ifndef TUPLEUTILS
#define TUPLEUTILS

#include <algorithm>
#include <tuple>
#include <utility>
using namespace std;

namespace impl {
    template <size_t I, size_t N, class... T>
    struct ZipI {
        constexpr decltype(auto) operator()(T&&... tup) {
            return tuple_cat(make_tuple(make_tuple(get<I>(forward<T>(tup))...)),
                            ZipI<I + 1, N, T...>()(forward<T>(tup)...));
        }
    };

    template <size_t N, class... T>
    struct ZipI<N, N, T...> {
        constexpr decltype(auto) operator()(T&&... tup) {
            return tuple<>();
        }
    };

    template <class... T>
    constexpr decltype(auto) zip(T&&... tup) {
        return ZipI<0, min({tuple_size<T>::value...}), T...>()(forward<T>(tup)...);
    }

    template <size_t... I, class T>
    constexpr decltype(auto) tuple_flattenIT(index_sequence<I...>, T&& t);

    template <class... T>
    constexpr decltype(auto) tuple_flatten(tuple<T...>&& t) {
        return tuple_flattenIT(make_index_sequence<sizeof...(T)>(), move(t));
    }

    template <class... T>
    constexpr decltype(auto) tuple_flatten(tuple<T...>& t) {
        return tuple_flattenIT(make_index_sequence<sizeof...(T)>(), t);
    }

    template <class... T>
    constexpr decltype(auto) tuple_flatten(const tuple<T...>& t) {
        return tuple_flattenIT(make_index_sequence<sizeof...(T)>(), t);
    }

    template <class T>
    constexpr decltype(auto) tuple_flatten(T&& t) {
        return make_tuple(forward<T>(t));
    }

    template <size_t... I, class T>
    constexpr decltype(auto) tuple_flattenIT(index_sequence<I...>, T&& t) {
        return tuple_cat(tuple_flatten(get<I>(forward<T>(t)))...);
    }
}

using impl::zip;
using impl::tuple_flatten;

#endif
